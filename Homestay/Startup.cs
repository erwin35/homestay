﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Homestay.Startup))]
namespace Homestay
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
