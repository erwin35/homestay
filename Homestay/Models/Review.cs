﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Homestay.Models
{
    public class Review
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required(ErrorMessage = "Please enter your review.")]
        [Display(Name = "Review")]
        public string ReviewContent { get; set; }

        [Required]
        [Display(Name = "Submitted On")]
        public DateTime CreatedOn { get; set; }
    }
}